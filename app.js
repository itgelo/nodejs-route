var http = require("http");
var url = require("url");
var router = require("routes")();

router.addRoute("/", function(req, res){
    res.writeHead(200, {"Content-Type":"text/plain"});
    res.end("Index page");
});

router.addRoute("/profile", function(req, res){
    res.writeHead(200, {"Content-Type":"text/plain"});
    res.end("Profile page");
});

router.addRoute("/route/:name?", function(req, res){
    res.writeHead(200, {"Content-Type":"text/plain"});
    res.end("Route page '" + this.params.name + "'");
});

http.createServer(function(req, res){
    var path = url.parse(req.url).pathname;
    var match = router.match(path);
    if(match){
        match.fn(req, res);
    }else {
        res.writeHead(404, {"Content-Type":"text/plain"});
        res.end("page not found!");
    }
}).listen(3000);
